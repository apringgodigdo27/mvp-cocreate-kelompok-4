import React from "react";
import { Link } from 'react-router-dom';
import { Button, Image } from "react-bootstrap";
import '../components/style.css';
import login from '../components/bg_login.png';

function SuccessAdmin() {
  return (
    <div>
    <div class="content-bg bg-admin">
        <img class="image-bg" src={login}/>
    </div>
        <div className="success-form">
        <div className="col-lg-5">
          <div className="card rounded-lg" >
            <div className="Login">
              <h3 className="text-center">Pendaftaran Berhasil</h3>              
              <form>
                <div class="text-center">
                  <h5 className="text-center">Silahkan periksa email anda untuk konfirmasi</h5>
                  <Button className="font-weight-bold my-3" variant="outline-primary" disabled>
                    <Link to="/loginAdmin">Kembali ke Login</Link>
                  </Button>
                </div>
              </form>
            </div>
          </div>  
        </div> 
       </div>
    </div>
  );
}

export default SuccessAdmin