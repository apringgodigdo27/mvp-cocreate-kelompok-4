import { React, useState } from 'react'
import headerIMG from "../assets/profileIMG.jpg";
import groupIMG from "../assets/groupIMG.jpg";
import '../components/style.css';
import login from '../components/bg_login.png';
import { Link } from 'react-router-dom';
import { Button } from "react-bootstrap";

export default function Group() {
  const [text, setText] = useState("");
  const [inputText, setInputText] = useState("");
  const [mode, setMode] = useState("view");

  const handleSave = () => {
    setMode("view");
  };

  const handleEdit = () => {
    setMode("edit");
  };

  return (
    <div>
      <div class="content-bg bg-user">
        <img class="image-bg" src={login}/>
      </div>
      
      <main className="container-fluid">
        <div className="row main-wrap ">
          <div className="col-sm-3">
            <div class="card w-100 my-4  text-center">
            <div className="card-body">
              <div className="row">
                <div className="col-2">
                  <img className="img-dashboard mt-1" src={headerIMG} />
                </div>
                <div className="col-8">
                    <h4 class="card-title">
                      Akai
                    </h4>
                    <p class="card-subtitle mb-2 text-muted">
                      Member
                    </p>
                </div>
              </div>
                <hr/>
              <h5><a href="/Dashboard"> Dashboard </a></h5>
              <hr/>
              <h5><a href="/Profile"> Profil</a></h5>
              <hr/>
              <h5><a href="/Group"> Grup </a></h5>
              <hr/>
              <h5><a href="/Login"> Keluar </a></h5>
            </div>
            </div>

            <div clasName=" col-sm-3 ">
              <div class="card w-100 my-4">
              <div className="card-body">
                <h5 className="card-header">Grup Teratas</h5><br/>
                  <h6 className="card-subtitle mb-2 text-muted">Total Grup (8)</h6>
                <hr/>
                <div className="d-flex flex-column">
                  <p className="card-text">1. UI <p className="card-subtitle mb-2 text-muted">(20 Anggota)</p></p>
                  <p className="card-text">2. FE <p className="card-subtitle mb-2 text-muted">(10 Anggota)</p></p>
                  <p className="card-text">3. BE <p className="card-subtitle mb-2 text-muted">(9 Anggota)</p></p>
                </div>
              </div>
            </div>
          </div>
        </div>

        
        <header className="col-sm-8 ">
            <div className="card my-4">
              <div className="card-body">
                <div className="row">
                  <div class="col-4">
                    <img class="img-grup" src={groupIMG} />
                  </div>
                  <div class="col">
                    <h3 class="card-title">
                      UI Group
                    </h3>
                    <p class="card-subtitle mb-2 text-muted">
                      Anggota
                    </p>
                    <h5 class="card-title">
                      Total Anggota (0)
                    </h5>
                    <h5 class="card-title">
                      Total Project (0)
                    </h5>
                    <p class="card-title">
                      Menyukai (1)
                    </p>
                  </div>
                  
                  <div className="col-sm-3">
                  <Button className="font-weight-bold my-3" variant="outline-danger">
                    <Link to="/Dashboard">Keluar Grup</Link>
                  </Button>
                  </div>
                </div>
              </div>
            </div>

          <main className="">
            <div className="card my-4">
              <div className="card-body">
                <h4 className="text-center">Detail Grup</h4>
                <hr/>
                <div class="card text-center">
            <div class="card-header">
                <ul class="nav nav-tabs card-header-tabs">
                <li class="nav-item">
                    <a class="nav-link active" href="#">Anggota</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Project</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Tentang</a>
                </li>
                </ul>
            </div>
            <div class="card-body">
                <h5 class="card-title">Grup belum memiliki anggota lainnya</h5>
            </div>
            </div>
              </div>
            </div>
          </main>
          </header>

          

            

        </div>
      </main>

    </div>
  )
}
