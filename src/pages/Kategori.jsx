import React from 'react';
import '../components/style.css';
import login from '../components/bg_login.png';

class Category extends React.Component{

    constructor(){
        super();
        this.state = {
            Category:[]
        }

        this.handleInputChange = this.handleInputChange.bind(this);
    }

    handleInputChange(event) {
        const target = event.target;
        var value = target.value;
        
        if(target.checked){
            this.state.Category[value] = value;   
        }else{
            this.state.Category.splice(value, 1);
        }
        
    }

    submit(){
        console.log(this.state)
    }

    render(){
        return(
          <div>
            <div class="content-bg bg-user">
              <img class="image-bg" src={login}/>
            </div>
            <div className="success-form">
                <div className="col-lg-5">
                  <div className="card rounded-lg">
                    <div class="card-body">
                        <h5 class="card-title">Beritahu Kami</h5>
                          <p class="card-text">
                            Apa yang membuat mu tertarik ?
                          </p>
                                  <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="Category" id="Software" value="Software" onChange={this.handleInputChange} />
                                    <label class="form-check-label" for="Software">Software</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="Category" id="Website" value="Website" onChange={this.handleInputChange} />
                                    <label class="form-check-label" for="Website">Website</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="Category" id="Hiburan" value="Hiburan" onChange={this.handleInputChange} />
                                    <label class="form-check-label" for="Hiburan">Hiburan</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="Category" id="Keuangan" value="Keuangan" onChange={this.handleInputChange} />
                                    <label class="form-check-label" for="Keuangan">Keuangan</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="Category" id="Banking" value="Banking" onChange={this.handleInputChange} />
                                    <label class="form-check-label" for="Banking">Banking</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="Category" id="Teknologi" value="Teknologi" onChange={this.handleInputChange} />
                                    <label class="form-check-label" for="Teknologi">Teknologi</label>
                                </div>
                            </div>
                        </div>
                        <a href="#" type="submit" class="btn btn-primary" onClick={()=>this.submit()}>Ke Dashboard</a>
                    </div>
                </div>
            </div>
        )  
    }
}

export default Category;