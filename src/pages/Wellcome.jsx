import React from "react";
import { Link } from 'react-router-dom';
import { Button, Image } from "react-bootstrap";
import '../components/style.css';
import login from '../components/bg_login.png';

function Wellcome() {
  return (
    <div>
    <div class="content-bg bg-user">
        <img class="image-bg" src={login}/>
    </div>
        <div className="success-form">
        <div className="col-lg-5">
          <div className="card rounded-lg" >
            <div className="Login">
              <h3 className="text-center">Ayo Gabung CoCreate</h3>              
              <form>
                <div class="text-center">
                  <h5 className="text-center">Temukan rekan dan buat bisnis anda sendiri sekarang !</h5>
                  <Button className="font-weight-bold my-3" variant="outline-primary" disabled>
                  <Link to="/login">Login</Link>
                </Button>
                </div>
                
                
              </form>
            </div>
          </div>  
        </div> 
       </div>
    </div>
  );
}

export default Wellcome