import Success from "./pages/Success";
import Dashboard from "./pages/Dashboard";
import Register from "./pages/Register";
import Login from "./pages/Login";
import Wellcome from "./pages/Wellcome";
import Profile from "./pages/Profile";
import Verifyemail from "./pages/Verivyemail";
import Kategori from "./pages/Kategori";
import Group from "./pages/Group";
import Coba from "./pages/Coba";
import Home from "./home/home";

import LoginAdmin from "./pages_admin/Login_admin"
import RegisAdmin from "./pages_admin/Regis_admin";
import SuccessAdmin from "./pages_admin/Success_admin";
import DashboardAdmin from "./pages_admin/Dashboard_admin";

import { Switch, Redirect, Route, BrowserRouter as Router } from "react-router-dom";

export default function App() {
  return (
    
    <Router>
      <Switch>
        <Route path="/coba"> <Coba /></Route>
        <Route path="/home"> <Home /></Route>
        <Route path="/login"> <Login /></Route>
        <Route path="/kategori"> <Kategori /></Route>
        <Route path="/dashboard"> <Dashboard /></Route>
        <Route path="/Group"> <Group /></Route>
        <Route path="/profile"> <Profile /></Route>
        <Route path="/register"> <Register /></Route>
        <Route path="/success"> <Success /></Route>
        <Route path="/verifyemail/:token"> <Verifyemail /></Route>
        <Route path="/loginAdmin"> <LoginAdmin /></Route>
        <Route path="/dashboardAdmin"> <DashboardAdmin /></Route>
        <Route path="/regisAdmin"> <RegisAdmin /></Route>
        <Route path="/successAdmin"> <SuccessAdmin /></Route>
      </Switch>
    </Router>
    
  );
}

