import React from 'react';
import "./assets/css/style.css";
import HeaderIMG from "./assets/images/banking.png";
import Slide1 from "./assets/images/slide1.png";
import Slide3 from "./assets/images/slide3.png";
import Icon1 from "./assets/images/avatar_icon_1.png";
import Icon2 from "./assets/images/avatar_icon_2.png";
import Icon3 from "./assets/images/avatar_icon_3.png";
import Icon4 from "./assets/images/avatar_icon_4.png";
import Icon5 from "./assets/images/avatar_icon_5.png";
import contentIMG from "./assets/images/content_image.jpg";

export default function home() {
    return (
    <section class="container">
      <div class="header-content">
          <div class="row">
              <div class="col-lg-2">
                  <img class="img-header" src={HeaderIMG} alt="banking"/>
              </div>
              <div class="col-lg-10">
                  <form action="javascript:void(0)">
                      <div class="row">
                          <div class="form-group col-lg-6 mt-2">
                              <input type="text" class="form-control form-shadow" id="search" placeholder="Temukan hal menarik disini..."/>
                          </div>
                          <div class="col-lg-4 ">
                          <ul class="row navul">
                            <li class="nav-item active">
                                <a class="nav-link" href="/Home">Home <span class="sr-only">(current)</span></a>
                            </li><li class="nav-item">
                                <a class="nav-link" href="/Profile">Profil</a>
                            </li><li class="nav-item">
                                <a class="nav-link" href="/Group">Grup</a>
                            </li>
                          </ul>
                          </div>
                          <div class="col-lg-2 mt-2">
                              <button href="/login" class="btn btn-primary btn-block rounded-pill">Keluar</button>
                          </div>
                      </div>
                  </form>
              </div>
          </div>
      </div>

      <div class="container content-slider mt-3 padding-50px">
          <h4 class="">Trending hari ini !</h4>
          <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
              <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
              </ol>
              <div class="carousel-inner">
                <div class="carousel-item active">
                  <img src={Slide1} class="d-block w-100" height="auto" alt="slide1"/>
                </div>
                <div class="carousel-item">
                  <img src={Slide3} class="d-block w-100 img-slide-rounded" height="auto" alt="slide3"/>
                </div>
              </div>
              <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
              </a>
              <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>
          </div>
      </div>
      <hr/>
      <div class="container my-3">
        <div class="col-mt-2 text-center">
          <div class="">
            <h4>Member Co.Create paling aktif</h4>
          </div>
          <div class="">
            <div class="avatar-icon float-center">
              <a href="#" class="img-link">
                <img src={Icon1} class="avatar avatar-80 " height="72px" width="72px"/>
              </a>
              <a href="#" class="img-link">
                <img src={Icon2} class="avatar avatar-80 " height="72px" width="72px"/>
              </a>
              <a href="#" class="img-link">
                <img src={Icon3} class="avatar avatar-80 " height="72px" width="72px"/>
              </a>
              <a href="#" class="img-link">
                <img src={Icon4} class="avatar avatar-80 " height="72px" width="72px"/>
              </a>
              <a href="#" class="img-link">
                <img src={Icon5} class="avatar avatar-80 " height="72px" width="72px"/>
              </a>
              <a href="#" class="img-link">
                <img src={Icon1} class="avatar avatar-80 " height="72px" width="72px"/>
              </a>
            </div>
          </div>
        </div>
        <hr/>
        <div class="row">
          <div class="col-lg-3">
            <ul class="navul">
              <li><a class="nav-link" href="#"><span class="fas fa-home"></span>Dashboard</a></li>
              <li><a class="nav-link" href="#"><span class="far fa-user"></span> Profil</a></li>
              <li><a class="nav-link" href="#"><span class="far fa-groups"></span> Grup</a></li><hr/>
              <li><a class="nav-link" href="#"><span class="far fa-lightbulb"></span> Tentang Co.Create</a></li>
            </ul>
            <div class="row mt-3">
              <div class="col-lg-4 text-center">
                <i class="fab fa-facebook-f fb-icon"></i>
              </div>
              <div class="col-lg-3">
                <i class="fab fa-twitter twitter-icon"></i>
              </div>
              <div class="col-lg-4">
                <i class="fab fa-google-plus google-icon"></i>
              </div>
            </div>
          </div>
          <div class="col-lg-9">
            <h4 class="content-title">Recommended for you</h4>
            <div class="row mt-4">
              <div class="col-lg-12">
                <div class="card card-content">
                  <div class="card-body">
                    <div class="row">
                      <div class="col-lg-4">
                        <span><img class="content-image" src={contentIMG} alt=""/></span>
                      </div>
                      <div class="col-lg-8">
                        <h5 class="card-title font-weight-bold">Youtube</h5>
                        <p class="card-text font-weight-normal">Reaksi Editor Indonesia</p>
                        <p class="font-weight-lighter author">Chandra Liow ~ 20 minutes ago</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="footer-component">
                  <span class="far fa-heart"></span> 2
                  <span class="far fa-comment-dots ml-3"></span> 0
                  <span class="fas fa-share-alt ml-3"></span>
                </div>
              </div>
            </div>
            <div class="row mt-4">
              <div class="col-lg-12">
                <div class="card card-content-green">
                  <div class="card-body">
                    <div class="row">
                      <div class="col-lg-4">
                        <span class="fas fa-book content-icon"></span>
                      </div>
                      <div class="col-lg-8">
                        <h5 class="card-title font-weight-bold">Ebook :</h5>
                        <p class="card-text font-weight-normal">
                          Digital Banking Era
                        </p>
                        <p class="font-weight-lighter author">Mpu Tantular ~ 2 hours ago</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="footer-component">
                  <span class="fas fa-heart text-danger"></span> 10
                  <span class="far fa-comment-dots ml-3"></span> 2
                  <span class="fas fa-share-alt ml-3"></span>
                </div>
              </div>
            </div>
            <div class="row mt-4">
              <div class="col-lg-12">
                <div class="card card-content">
                  <div class="card-body">
                    <div class="row">
                      <div class="col-lg-4">
                        <span class="far fa-edit content-icon"></span>
                      </div>
                      <div class="col-lg-8">
                        <h5 class="card-title font-weight-bold">Project Prototype :</h5>
                        <p class="card-text font-weight-normal">
                          Financial Analyst Mobile Apps
                        </p>
                        <p class="font-weight-lighter author">CodingFun ~ 1 week ago</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="footer-component">
                  <span class="fas fa-heart text-danger"></span> 
                  <span class="far fa-comment-dots ml-3"></span>
                  <span class="fas fa-share-alt ml-3"></span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    )
}
